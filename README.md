# Miles Russell

## About Me

I'm a manager on the Enterprise Data Team at GitLab. I live in Omaha, Nebraska, USA
with my wife, two sons, and dog. My educational background is in Economics, but I fell into
a data analysis internship while in grad school and I've been in the field ever since!
When I'm not working, I love to spend time with my family, golf, play chess or
video games, listen to music, hike, camp, and watch football. That's about all I
have room for in my life! I don't like social media but I do use LinkedIn
professionally. So feel free to [connect with me](https://www.linkedin.com/in/miles-russell-288485ab/) if you like.

## How I Work

I have a nonlinear work day, with work hours interspersed 8am-5pm US CT and 8pm-11pm US CT. 
Since GitLab is fully remote and asyncronous, please consider if a meeting is necessary before scheduling one. 
If you do schedule one, please share a meeting agenda beforehand.

# Leadership User Manual
I am adding this to my README as part of my Elevate Leadership Development Program.

- One of my top values at work is:
    - Beyond GitLab's CREDIT values (which I admire) a personal work value of mine is _**Customer Service**_. It is rare for members of a Data Team to interface directly with external 
      customers, but we are still relied upon to produce a high quality service. Our coworkers and our business partners are our customers, and we should work hard to make sure that
      they have a great experience with data.
- Something you should know about working with me is:
    - I earnestly try my best.
- One of my top strengths at work is:
    - Organization. I am a very organized person, and I leverage that quality at work to help my team increase our Transparency and (hopefully) Results.
- An area of growth I'm focused on right now is:
    - Facilitating cross-functional alignment
    - Working with business partners to build roadmaps 3-4 quarters in advance
    - Leading a team that maintains aspirational, gold standard, A+ data products
- Something that often frustrates me at work is:
    - I can get frustrated when someone doesn't follow through on what they said they would do. It is okay to run behind and ask for more time; it is okay to communicate when priorities,
      expectations, or timelines need to change.
- How I'd like to recieve feedback is:
    - I appreciate direct feedback as soon as it is appropriate to deliver it. I aspire to have short toes and a small ego, so you won't hurt my feelings.
- One of my proudest moments at work is:
    - One time Sid called a suggestion of mine "genius". I don't think I'll forget that as long as I live.
